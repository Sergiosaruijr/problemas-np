

def eh_possivel_transportar(objetos, transportes) -> bool:
    soma_objetos = sum(objetos)
    soma_suporta_transportes = sum(transportes)
    if(soma_suporta_transportes > soma_objetos):
        print("É possível transportar a carga.")
    else:
        print("Não é possível transportar a carga.")


if __name__ == '__main__':
    objetos = [int(i) for i in input().split(',')]
    transportes = [int(i) for i in input().split(',')]
    eh_possivel_transportar(objetos,transportes)
