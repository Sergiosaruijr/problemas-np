sudoku = [[5,3,0,0,7,0,0,0,0],
         [6,0,0,1,9,5,0,0,0],
         [0,9,8,0,0,0,0,6,0],
         [8,0,0,0,6,0,0,0,3],
         [4,0,0,8,0,3,0,0,1],
         [7,0,0,0,2,0,0,0,6],
         [0,6,0,0,0,0,2,8,0],
         [0,0,0,4,1,9,0,0,5],
         [0,0,0,0,8,0,0,7,9]]


def nap_ha_violacao(linha, coluna, n):
    global sudoku
    #Existe o n na linha?
    for i in range(0,9):
        if sudoku[linha][i] == n:
            return False

    #Existe o n na coluna?
    for i in range(0,9):
        if sudoku[i][coluna] == n:
            return False
    
    #Existe o n no setor(quadrado)?
    x0 = (coluna // 3) * 3
    y0 = (linha // 3) * 3
    for i in range(0,3):
        for j in range(0,3):
            if sudoku[y0+i][x0+j] == n:
                return False

    return True


def print_do_sudoku(sudoku):
    for i in range(len(sudoku)):
        if i % 3 == 0 and i != 0:
            print("- - - - - - - - - - - - - ")

        for j in range(len(sudoku[0])):
            if j % 3 == 0 and j != 0:
                print(" | ", end="")

            if j == 8:
                print(sudoku[i][j])
            else:
                print(str(sudoku[i][j]) + " ", end="")   

def imprime():
    global sudoku
    for linha in range(0,9):
        for coluna in range(0,9):
            if sudoku[linha][coluna] == 0:
                for n in range(1,10):
                    if nap_ha_violacao(linha, coluna, n):
                        sudoku[linha][coluna] = n
                        imprime()
                        sudoku[linha][coluna] = 0

                return
    print_do_sudoku(sudoku)
    




if __name__ == '__main__':
    imprime()
